package org.tds.sgh.business;


import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.helper.DateHelper;
import org.tds.sgh.infrastructure.ICalendario;
import org.tds.sgh.infrastructure.Infrastructure;

public class Reserva {
	
	// --------------------------------------------------------------------------------------------
	
	private String codigo;
	private GregorianCalendar fechaInicio;
	private GregorianCalendar fechaFin;
	private boolean esModificable;
	private EstadoReserva estado;
	private Cliente cliente;
	private Habitacion habitacion;
	private TipoHabitacion tipoHabitacion;
	private Map<String, Huesped> huespedes;
	private Hotel hotel;
	
	

	
	// --------------------------------------------------------------------------------------------

	public Reserva( String codigo ) {
		this.codigo = codigo;
		this.huespedes = new HashMap<String, Huesped>();
		//this.huespedes = Collections.<HuespedDTO>emptyList()
		//this.huespedes = null;
	}

	
	// --------------------------------------------------------------------------------------------


	public Reserva() {
		// TODO Auto-generated constructor stub
		this.huespedes = new HashMap<String, Huesped>();
	}


	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public GregorianCalendar getFechaInicio() {
		return fechaInicio;
	}



	public void setFechaInicio(GregorianCalendar fechaInicio) {
		this.fechaInicio = fechaInicio;
	}



	public GregorianCalendar getFechaFin() {
		return fechaFin;
	}



	public void setFechaFin(GregorianCalendar fechaFin) {
		this.fechaFin = fechaFin;
	}



	public boolean getEsModificable() {
		return esModificable;
	}



	public void setEsModificable(boolean esModificable) {
		this.esModificable = esModificable;
	}



	public EstadoReserva getEstado() {
		return estado;
	}



	public void setEstado(EstadoReserva estado) {
		this.estado = estado;
	}



	public Cliente getCliente() {
		return cliente;
	}



	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}



	public Habitacion getHabitacion() {
		return habitacion;
	}



	public void setHabitacion(Habitacion habitacion) {
		this.habitacion = habitacion;
	}



	public TipoHabitacion getTipoHabitacion() {
		return tipoHabitacion;
	}



	public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
		this.tipoHabitacion = tipoHabitacion;
	}



	public Map<String, Huesped> getHuespedes() {
		return huespedes;
	}



	public void setHuespedes(Map<String, Huesped> huespedes) {
		this.huespedes = huespedes;
	}
	
	
	public Set<Huesped> listarHuespedes()
	{
		return new HashSet<Huesped>(this.huespedes.values());
	} 


	public Hotel getHotel() {
		return hotel;
	}


	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	/**
	 * Comprueba si la reserva es diponible en base al tipo de habitacion y un rango de fechas.
	 * @param tipoHabitacionBusqueda
	 * @param fechaInicioBusqueda
	 * @param fechaFinBusqueda
	 * @return
	 */
	public boolean coincideBusqueda(TipoHabitacion tipoHabitacionBusqueda, GregorianCalendar fechaInicioBusqueda, GregorianCalendar fechaFinBusqueda) {
		
		System.out.println( "fechaInicioBusqueda: " +DateHelper.format(fechaInicioBusqueda)  );
		System.out.println( "fechaFinBusqueda: " + DateHelper.format(fechaFinBusqueda)  );
		
		System.out.println( "fechaInicio: " +DateHelper.format(this.fechaInicio)  );
		System.out.println( "fechaFin: " +DateHelper.format(this.fechaFin)  );
		
		
//		if(this.tipoHabitacion.getNombre().equals( tipoHabitacionBusqueda.getNombre())) {
//				
//			if((Infrastructure.getInstance().getCalendario().esAnterior( this.fechaFin , fechaInicioBusqueda ) ||
//						Infrastructure.getInstance().getCalendario().esMismoDia(this.fechaFin, fechaInicioBusqueda))  )
//			{
//				return false;
//				
//			}
//			else if( Infrastructure.getInstance().getCalendario().esPosterior( this.fechaInicio,  fechaFinBusqueda ) ) {
//				return false;
//			}
//		}
//		
//		return true;
//		
		
 		return this.tipoHabitacion.getNombre().equals( tipoHabitacionBusqueda.getNombre()) &&
 				
 				!( 
 						(Infrastructure.getInstance().getCalendario().esAnterior( this.fechaFin , fechaInicioBusqueda ) ||
 							Infrastructure.getInstance().getCalendario().esMismoDia(this.fechaFin, fechaInicioBusqueda)) 
 						
 						|| 				
 						Infrastructure.getInstance().getCalendario().esPosterior( this.fechaInicio,  fechaFinBusqueda ) );
 				
	}
	
	public void agregarHuesped(String nombre, String documento) throws Exception {
		 Huesped newHuesped = new Huesped(nombre, documento);
		 huespedes.put(documento, newHuesped);
    }
	
	public boolean coincideCodigo(String codigo) {
		return codigo.equals(getCodigo());
	}
	
	public boolean estaPendiente() {
		return getEstado().toString().equals(EstadoReserva.PENDIENTE.toString());
	}
	
	public boolean esMismoDia(GregorianCalendar fechaActual) 
	{
		ICalendario calendar = Infrastructure.getInstance().getCalendario();
		
		return ((calendar.esPosterior(fechaActual, getFechaInicio()) || calendar.esMismoDia(fechaActual, getFechaInicio()))
			&&
			(calendar.esAnterior(fechaActual, getFechaFin()) || calendar.esMismoDia(fechaActual, getFechaFin()))
		  	);
			
	}
	
	public boolean estaReservaTomada() {
         	return getEstado().toString().equalsIgnoreCase(EstadoReserva.TOMADA.toString());	
	}
}
