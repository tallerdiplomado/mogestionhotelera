package org.tds.sgh.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.tds.sgh.helper.DateHelper;
import org.tds.sgh.infrastructure.Infrastructure;


public class Hotel
{
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Habitacion> habitaciones;
	
	private String nombre;
	
	private String pais;
	
	private Map<String, Reserva> reservas;
	
	
	
	// --------------------------------------------------------------------------------------------
	
	public Hotel(String nombre, String pais)
	{
		this.habitaciones = new HashMap<String, Habitacion>();
		
		this.reservas = new HashMap<String, Reserva>();
		
		this.nombre = nombre;
		
		this.pais = pais;
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Habitacion agregarHabitacion(TipoHabitacion tipoHabitacion, String nombre) throws Exception
	{
		if (this.habitaciones.containsKey(nombre))
		{
			throw new Exception("El hotel ya tiene una habitación con el nombre indicado.");
		}
		
		Habitacion habitacion = new Habitacion(tipoHabitacion, nombre);
		
		this.habitaciones.put(habitacion.getNombre(), habitacion);
		
		return habitacion;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public String getPais()
	{
		return this.pais;
	}
	
	public Set<Habitacion> listarHabitaciones()
	{
		return new HashSet<Habitacion>(this.habitaciones.values());
	}

	public Map<String, Reserva> getReservas() {
		return reservas;
	}

	public void setReservas(Map<String, Reserva> reservas) {
		this.reservas = reservas;
	}

	/**
	 * Devuelve true si es que hay disponibilidad para reservar 
	 * @param tipoHabitacion
	 * @param fechaInicio
	 * @param fechaFin
	 * @return
	 */
	public boolean confirmarDisponibilidad(TipoHabitacion tipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin) {
	
		// Habitaciones que coinciden con el tipo de habitacion buscado ... 
		Set<Habitacion> habitacionesTipo = new HashSet<>();
		int cantidadHabitacionesNoDisponibles = 0;
		
		for( Map.Entry<String, Habitacion> hab: this.habitaciones.entrySet() ) {
			if( hab.getValue().getTipoHabitacion().getNombre().equals( tipoHabitacion.getNombre() ) )
				habitacionesTipo.add(hab.getValue());	
		}
		
		for( Map.Entry<String, Reserva> reserva: this.reservas.entrySet() ) {
			
			if( reserva.getValue().coincideBusqueda( tipoHabitacion, fechaInicio, fechaFin ) )  //getTipoHabitacion();
				cantidadHabitacionesNoDisponibles ++;
		}
		
		return cantidadHabitacionesNoDisponibles < habitacionesTipo.size();
	}
	
	public Reserva buscarReserva(String codigo) {
		Reserva reserva = reservas.get(codigo);
		return reserva;
	}
	
	public Set<Reserva> buscarReservasPendientes(){
		
		Set<Reserva> retReserva = new HashSet<>() ;
		
		for (Map.Entry<String, Reserva> element : this.reservas.entrySet()) {
			if (element.getValue().getEstado().toString().equals(EstadoReserva.PENDIENTE.toString()) ) {
				
				if(element.getValue().esMismoDia(Infrastructure.getInstance().getCalendario().getHoy()))
				{
					retReserva.add(element.getValue());
				}
			}
		}		
		return retReserva;
	}
	
	public Reserva seleccionarReserva(String codigoReserva) {
		for (Map.Entry<String, Reserva> element : this.reservas.entrySet()) {
			if(element.getValue().coincideCodigo(codigoReserva))
			{
				return element.getValue();
			}
		}
		return null;
	}

	public Reserva crearReserva(
			Cliente cliente, 
			Hotel hotel, 
			TipoHabitacion tipoHabitacion, 
			GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, 
			boolean modificablePorHuesped) {
		
		Reserva reserva = new Reserva( (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE) + "" );
		reserva.setCliente(cliente); 
		reserva.setEstado( EstadoReserva.PENDIENTE );
		reserva.setFechaInicio(fechaInicio);
		reserva.setFechaFin(fechaFin);
		reserva.setEsModificable(modificablePorHuesped);
		reserva.setHotel(hotel);
		reserva.setTipoHabitacion(tipoHabitacion);
		
		this.reservas.put( reserva.getCodigo() , reserva);
		
		return reserva;
	}
	
	public Set<Reserva> obtenerReservasPendientes(Cliente c){
		Set<Reserva> retReserva = new HashSet<>() ;
		for (Map.Entry<String, Reserva> element : this.reservas.entrySet()) {
			if(element.getValue().getCliente().getRut().equalsIgnoreCase(c.getRut()))
			{
				
				System.out.println( "fechaInicio: " +DateHelper.format( element.getValue().getFechaInicio() )  );
				System.out.println( "fechaFin: " +DateHelper.format(element.getValue().getFechaFin())  );
				System.out.println( "Hoy: " +DateHelper.format( Infrastructure.getInstance().getCalendario().getHoy())  );
				
				
				if (element.getValue().estaPendiente() &&
						(Infrastructure.getInstance().getCalendario().esFutura(element.getValue().getFechaInicio() ) ||
								Infrastructure.getInstance().getCalendario().esHoy( element.getValue().getFechaInicio() )	) ) {
			    	retReserva.add(element.getValue());
				}
			}
		}
		return retReserva;		
	}
	
	public Reserva tomarReserva(String reserva) {
		Reserva res = this.buscarReserva(reserva);
		List<Habitacion> habPosibles = new ArrayList<Habitacion>();
		for (Map.Entry<String, Habitacion> hab : this.habitaciones.entrySet()) {
			if (res.getTipoHabitacion().getNombre().
					equalsIgnoreCase(hab.getValue().getTipoHabitacion().getNombre())) {
				habPosibles.add(hab.getValue());
			}			
		}
		List<Habitacion> habTomadas= new ArrayList<Habitacion>();
		for (Map.Entry<String, Reserva> r : this.reservas.entrySet()) {
			if (r.getValue().estaReservaTomada()	&& 
				r.getValue().getTipoHabitacion().getNombre().equals(res.getTipoHabitacion().getNombre())) {
				habTomadas.add(r.getValue().getHabitacion());
			}
		}
		//habCandidates = habPosibles - habTomadas
		List<Habitacion> habCandidates = new ArrayList<Habitacion>();
		for (Iterator iterator = habPosibles.iterator(); iterator.hasNext();) {
			Habitacion habPosible = (Habitacion) iterator.next();
			if( habTomadas.isEmpty() )
			{
				habCandidates.addAll(habPosibles);
			}else {
			
				for (Iterator iterator2 = habTomadas.iterator(); iterator2.hasNext();) {
					Habitacion habTomada = (Habitacion) iterator2.next();
					if (!habPosible.getNombre().equalsIgnoreCase(habTomada.getNombre())) {
						habCandidates.add(habPosible);
					}
				}
			
			}
		}
		if (habCandidates.size()>0) {
			res.setHabitacion(habCandidates.get(0));
			res.setEstado( EstadoReserva.TOMADA );
		}
		return res;
		
	}
	/*
	public Reserva cancelarReservasNoTomadas(String cliente) {		
		for (Map.Entry<String, Reserva> res : this.reservas.entrySet()) {
			if (res.getValue().getCliente().getRut().equalsIgnoreCase(cliente)
					&& Infrastructure.getInstance().getCalendario().esPosterior(
					Infrastructure.getInstance().getCalendario().getHoy(), res.getValue().getFechaInicio())) {
				res.getValue().setEstado(EstadoReserva.CANCELADA);
				return res.getValue();
			}
		}
		return new Reserva();
	}
*/
}
