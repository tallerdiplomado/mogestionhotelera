package org.tds.sgh.business;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.tds.sgh.infrastructure.Infrastructure;

public class CadenaHotelera
{
	// --------------------------------------------------------------------------------------------
	
	private Map<String, Cliente> clientes;
	
	private Map<String, Hotel> hoteles;
	
	private String nombre;
	
	private Map<String, TipoHabitacion> tiposHabitacion;
	
	// --------------------------------------------------------------------------------------------
	
	public CadenaHotelera(String nombre)
	{
		this.clientes = new HashMap<String, Cliente>();
		
		this.hoteles = new HashMap<String, Hotel>();
		
		this.nombre = nombre;
		
		this.tiposHabitacion = new HashMap<String, TipoHabitacion>();
	}
	
	// --------------------------------------------------------------------------------------------
	
	public Cliente agregarCliente(
		String rut,
		String nombre,
		String direccion,
		String telefono,
		String mail) throws Exception
	{
		if (this.clientes.containsKey(rut))
		{
			throw new Exception("Ya existe un cliente con el RUT indicado.");
		}
		
		Cliente cliente = new Cliente(rut, nombre, direccion, telefono, mail);
		
		this.clientes.put(cliente.getRut(), cliente);
		
		return cliente;
	}
	
	public Hotel agregarHotel(String nombre, String pais) throws Exception
	{
		if (this.hoteles.containsKey(nombre))
		{
			throw new Exception("Ya existe un hotel con el nombre indicado.");
		}
		
		Hotel hotel = new Hotel(nombre, pais);
		
		this.hoteles.put(hotel.getNombre(), hotel);
		
		return hotel;
	}
	
	public TipoHabitacion agregarTipoHabitacion(String nombre) throws Exception
	{
		if (this.tiposHabitacion.containsKey(nombre))
		{
			throw new Exception("Ya existe un tipo de habitación con el nombre indicado.");
		}
		
		TipoHabitacion tipoHabitacion = new TipoHabitacion(nombre);
		
		this.tiposHabitacion.put(tipoHabitacion.getNombre(), tipoHabitacion);
		
		return tipoHabitacion;
	}
	
	public Cliente buscarCliente(String rut) throws Exception
	{
		Cliente cliente = this.clientes.get(rut);
		
		if (cliente == null)
		{		
			throw new Exception("No existe un cliente con el nombre indicado.");
		}
		
		return cliente;
	}
	
	public Set<Cliente> buscarClientes(String patronNombreCliente) {
		Set<Cliente> clientesEncontrados = new HashSet<Cliente>();
		if (patronNombreCliente == null) {
			throw new NullPointerException();
		}
		for (Cliente cliente : this.clientes.values()) {
			if (cliente.coincideElNombre(patronNombreCliente)) {
				clientesEncontrados.add(cliente);
			}
		}

		return clientesEncontrados;
	}
	
	public Hotel buscarHotel(String nombre) throws Exception
	{
		Hotel hotel = this.hoteles.get(nombre);
		
		if (hotel == null)
		{
			throw new Exception("No existe un hotel con el nombre indicado.");
		}
		
		return hotel;
	}
	
	public TipoHabitacion buscarTipoHabitacion(String nombre) throws Exception
	{
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombre);
		
		if (tipoHabitacion == null)
		{
			throw new Exception("No existe un tipo de habitación con el nombre indicado.");
		}
		
		return tipoHabitacion;
	}
	
	public String getNombre()
	{
		return this.nombre;
	}
	
	public Set<Cliente> listarClientes()
	{
		return new HashSet<Cliente>(this.clientes.values());
	}
	
	public Set<Hotel> listarHoteles()
	{
		return new HashSet<Hotel>(this.hoteles.values());
	}
	
	public Set<TipoHabitacion> listarTiposHabitacion()
	{
		return new HashSet<TipoHabitacion>(this.tiposHabitacion.values());
	}

	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception {
		
		Hotel hotel = this.hoteles.get( nombreHotel );
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get( nombreTipoHabitacion );
		
		if( Infrastructure.getInstance().getCalendario().esPasada( fechaInicio ) ) {
			throw new Exception( "La fecha es anterior a la fecha actual" );
		}
		
		if( Infrastructure.getInstance().getCalendario().esPosterior(fechaInicio, fechaFin) )
		{
			throw new Exception( "La fecha inicio es posterior a la fecha fin" );
		}
		
		if(null==tipoHabitacion) {
			throw new Exception("No existe tipo de Habitacion");
		}
		return hotel.confirmarDisponibilidad( tipoHabitacion, fechaInicio, fechaFin );
	}
	
	public Reserva registrarHuesped(String nombre, String documento, String codigoReserva) throws Exception {
		Reserva reserva = new Reserva();
		for (Iterator iterator = listarHoteles().iterator(); iterator.hasNext();) {
			Hotel hotel = (Hotel) iterator.next();
	        reserva =  hotel.buscarReserva(codigoReserva);
		}
		reserva.agregarHuesped(nombre, documento);
		return reserva;
	}

	public Reserva registrarReserva(
			String rutCliente, 
			String nombreHotel, 
			String nombreTipoHabitacion, 
			GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, 
			boolean modificablePorHuesped) {
		
		Cliente cliente = this.clientes.get(rutCliente);
		Hotel hotel = this.hoteles.get(nombreHotel);
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombreTipoHabitacion);
		
		Reserva reserva = hotel.crearReserva(
				cliente,
				hotel,
				tipoHabitacion,  
				fechaInicio,
				fechaFin,  
				modificablePorHuesped );
		
		Infrastructure.getInstance().getSistemaMensajeria().enviarMail(cliente.getMail(), "Reserva creada ", 
				reserva.getCodigo());
	
		hotel.getReservas().put( reserva.getCodigo(), reserva );
		return reserva;
	}

	
	public Set<Hotel> sugerirAlternativas(
			String pais, 
			String nombreTipoHabitacion, 
			GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin) {
		
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get( nombreTipoHabitacion );
		Set<Hotel> hotelesSugeridos = new HashSet<>();
		
		for( Map.Entry<String, Hotel> hotel: this.hoteles.entrySet() ) {
			
			if ( hotel.getValue().getPais().equals( pais ) && 
					hotel.getValue().confirmarDisponibilidad(tipoHabitacion, fechaInicio, fechaFin) ) {
				hotelesSugeridos.add(hotel.getValue());
			}
		}
		return hotelesSugeridos;
	}
	
	
	public Set<Reserva> buscarReservasPendientes(String hotel){				
		
		for (Map.Entry<String, Hotel> eHotel : this.hoteles.entrySet()) {
			
			if(eHotel.getValue().getNombre().equals(hotel))
			{
				return eHotel.getValue().buscarReservasPendientes();
			}
			
		}
		return  new HashSet<>() ;

	}
	
	public Reserva seleccionarReserva(String codigoReserva) {
		for (Map.Entry<String, Hotel> element : this.hoteles.entrySet()) {
		 	Reserva reserva = element.getValue().seleccionarReserva(codigoReserva);
		 	if (reserva != null ) {
		 		return reserva;
		 	}
		}
		return new Reserva();
	}
	
	public Set<Reserva> buscarReservasDelCliente (String cliente){
		Cliente c = this.clientes.get(cliente);
		Set<Reserva> retReserva = new HashSet<>() ;
		for (Map.Entry<String, Hotel> element : this.hoteles.entrySet()) {
			retReserva.addAll(element.getValue().obtenerReservasPendientes(c));
		}
		return retReserva;
	}


	public Reserva modificarReserva(
			String rutCliente, 
			String codigoReserva,
			String nombreHotel, 
			String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, 
			GregorianCalendar fechaFin, 
			boolean modificablePorHuesped) throws Exception {
	
		Cliente cliente = this.buscarCliente(rutCliente);
		Hotel hotel = this.hoteles.get(nombreHotel);
		TipoHabitacion tipoHabitacion = this.tiposHabitacion.get(nombreTipoHabitacion);
		
		Reserva reserva = this.seleccionarReserva(String.valueOf( codigoReserva ));
		
		if( this.confirmarDisponibilidad(nombreHotel, nombreTipoHabitacion, fechaInicio, fechaFin) ) {			
			
			
			
			reserva.setEsModificable(modificablePorHuesped);
			reserva.setFechaFin(fechaFin);
			reserva.setFechaInicio(fechaInicio);
			
			reserva.setTipoHabitacion(tipoHabitacion);
			
			if( !nombreHotel.equals( reserva.getHotel().getNombre() ) ) {
				reserva.getHotel().getReservas().remove( reserva.getCodigo() );
				hotel.getReservas().put(reserva.getCodigo(), reserva);
				reserva.setHotel(hotel);
			}
			
			
			
			Infrastructure.getInstance().getSistemaMensajeria().enviarMail(
					cliente.getMail(), 
					"Reserva modificada ", 
					reserva.getCodigo());
			
		}
		return reserva;
   }	

	
	public Reserva tomarReserva(String reserva) {
		for (Map.Entry<String, Hotel> element : this.hoteles.entrySet()) {
		 	Reserva r = element.getValue().seleccionarReserva(reserva);
		 	if (r != null ) {
		 		return element.getValue().tomarReserva(r.getCodigo());	 		
		 	}
		}
		return new Reserva();
	}
	
	public Reserva cancelarReservasNoTomadas(String cliente) {

		Set<Reserva> reservas = buscarReservasDelCliente(cliente);
		for (Iterator iterator = reservas.iterator(); iterator.hasNext();) {
			Reserva reserva = (Reserva) iterator.next();
			if (Infrastructure.getInstance().getCalendario()
					.esAnterior(Infrastructure.getInstance().getCalendario().getHoy(), reserva.getFechaInicio())) {
				reserva.setEstado(EstadoReserva.CANCELADA);
				Infrastructure.getInstance().getSistemaMensajeria().enviarMail(reserva.getCliente().getMail(),
						"cancelar reserva", "ud cancelo reserva");
				return reserva;
			}
		}
		return new Reserva();
	}
}
