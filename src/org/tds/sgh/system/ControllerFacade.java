/**
 * 
 */
package org.tds.sgh.system;

import java.util.GregorianCalendar;
import java.util.Set;

import org.tds.sgh.business.CadenaHotelera;
import org.tds.sgh.business.Cliente;
import org.tds.sgh.business.Hotel;
import org.tds.sgh.business.Reserva;
import org.tds.sgh.dtos.ClienteDTO;
import org.tds.sgh.dtos.DTO;
import org.tds.sgh.dtos.HotelDTO;
import org.tds.sgh.dtos.ReservaDTO;
import org.tds.sgh.infrastructure.Infrastructure;

/**
 * @author juanfrancisco
 *
 */
public class ControllerFacade implements ITomarReservaController, ICancelarReservaController {

	
	private CadenaHotelera cadenaHotelera;
	private String rutCliente;
	private String codigoReserva;
	
	public ControllerFacade( CadenaHotelera cadenaHotelera ) {
		this.cadenaHotelera = cadenaHotelera;
	}
	
	
	public Set<ReservaDTO> buscarReservasDelCliente () throws Exception{
		
		if(null == this.rutCliente ) {
			throw new Exception( "No existe el cliente" );
		}
		
		Set<Reserva> reservas = this.cadenaHotelera.buscarReservasDelCliente(this.rutCliente);
		return DTO.getInstance().mapReservas(reservas);
	}

	@Override
	public Set<ClienteDTO> buscarCliente(String patronNombreCliente) {

		Set<Cliente> clientes = this.cadenaHotelera.buscarClientes(patronNombreCliente);
		return DTO.getInstance().mapClientes(clientes);
	}

	@Override
	public ClienteDTO seleccionarCliente(String rut) throws Exception {
		Cliente cliente = this.cadenaHotelera.buscarCliente(rut);
		
		this.setRutCliente( cliente.getRut() ); // TODO: Validar que el rut no sea nulo!
		
		return DTO.getInstance().map(cliente);
	}

	@Override
	public boolean confirmarDisponibilidad(String nombreHotel, String nombreTipoHabitacion,
			GregorianCalendar fechaInicio, GregorianCalendar fechaFin) throws Exception {
		
		
		return this.cadenaHotelera.confirmarDisponibilidad( nombreHotel,  nombreTipoHabitacion,
				 fechaInicio,  fechaFin);
	}

	@Override
	public ReservaDTO registrarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		
		Reserva reserva = this.cadenaHotelera.registrarReserva(
				this.rutCliente, // TODO: Validar que el rut no sea nulo 
				nombreHotel,  
				nombreTipoHabitacion,  
				fechaInicio,
				fechaFin,  
				modificablePorHuesped );
		
		this.codigoReserva = reserva.getCodigo();
	
		return DTO.getInstance().map(reserva);
	}

	@Override
	public Set<HotelDTO> sugerirAlternativas(String pais, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin) throws Exception {

		
		Set<Hotel> hoteles =  this.cadenaHotelera.sugerirAlternativas( 
				pais,  
				nombreTipoHabitacion,  
				fechaInicio,
				fechaFin);
				
		return DTO.getInstance().mapHoteles(hoteles);
	}

	@Override
	public ClienteDTO registrarCliente(String rut, String nombre, String direccion, String telefono, String mail)
			throws Exception {
		
		Cliente cliente = this.cadenaHotelera.agregarCliente(rut, nombre, direccion, telefono, mail);
		this.rutCliente = cliente.getRut();
		return DTO.getInstance().map(cliente);
	}

	@Override
	public ReservaDTO modificarReserva(String nombreHotel, String nombreTipoHabitacion, GregorianCalendar fechaInicio,
			GregorianCalendar fechaFin, boolean modificablePorHuesped) throws Exception {
		
		Reserva reserva = this.cadenaHotelera.modificarReserva( 
				this.rutCliente,
				this.codigoReserva,
				nombreHotel, 
				nombreTipoHabitacion, 
				fechaInicio,
				fechaFin, 
				modificablePorHuesped );
		
		return DTO.getInstance().map(reserva);
	}

	@Override
	public ReservaDTO cancelarReservaDelCliente() throws Exception {
		Reserva reserva = cadenaHotelera.cancelarReservasNoTomadas(this.rutCliente); 
		return DTO.getInstance().map(reserva);
	}

	@Override
	public Set<ReservaDTO> buscarReservasPendientes(String nombreHotel) throws Exception {		
		Set<Reserva> reservasPendientes = cadenaHotelera.buscarReservasPendientes(nombreHotel);
		return DTO.getInstance().mapReservas(reservasPendientes);
		
	}

	@Override
	public ReservaDTO seleccionarReserva(long codigoReserva) throws Exception {
		Reserva reserva = cadenaHotelera.seleccionarReserva(String.valueOf(codigoReserva));		
		this.codigoReserva = reserva.getCodigo();
		if (rutCliente != null && !reserva.getCliente().getRut().equalsIgnoreCase(this.rutCliente)) {
			throw new Exception("Selecciona reserva de otro cliente");
	    }
		return DTO.getInstance().map(reserva);
	}

	@Override
	public ReservaDTO registrarHuesped(String nombre, String documento) throws Exception {
		Reserva reserva = this.cadenaHotelera.registrarHuesped(nombre, documento, this.codigoReserva); 
		return DTO.getInstance().map(reserva);
	}

	@Override
	public ReservaDTO tomarReserva() throws Exception {
		Reserva reserva = this.cadenaHotelera.tomarReserva(codigoReserva);
		ReservaDTO reservaDto = DTO.getInstance().map(reserva);
		if (reserva.getHabitacion() != null) {
			Infrastructure.getInstance().getSistemaMensajeria().enviarMail(reserva.getCliente().getMail(),
					"tomar reserva", "se realiza check-in");
			Infrastructure.getInstance().getSistemaFacturacion().iniciarEstadia(reservaDto);
		}
		return reservaDto;
	}

	public CadenaHotelera getCadenaHotelera() {
		return cadenaHotelera;
	}


	public void setCadenaHotelera(CadenaHotelera cadenaHotelera) {
		this.cadenaHotelera = cadenaHotelera;
	}


	public String getRutCliente() {
		return rutCliente;
	}


	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}


	public String getCodigoReserva() {
		return codigoReserva;
	}


	public void setCodigoReserva(String codigoReserva) {
		this.codigoReserva = codigoReserva;
	}
	

}
